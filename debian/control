Source: emacs25
Section: editors
Priority: optional
Maintainer: Rob Browning <rlb@defaultvalue.org>
Build-Depends: bsd-mailx | mailx, libncurses5-dev, texinfo, liblockfile-dev, librsvg2-dev,
 dbus-x11,
 gnupg-agent,
 libgif-dev | libungif4-dev,
 libtiff-dev,
 procps,
 xaw3dg-dev,
 libpng-dev, libjpeg-dev, libm17n-dev, libotf-dev,
 libgpm-dev [linux-any], libdbus-1-dev,
 autoconf, automake, autotools-dev, dpkg-dev (>> 1.10.0), quilt (>= 0.42),
 debhelper (>= 9), libxaw7-dev, sharutils, imagemagick, libgtk-3-dev,
 libgnutls28-dev, libxml2-dev, libselinux1-dev [linux-any],
 libasound2-dev [!hurd-i386 !kfreebsd-i386 !kfreebsd-amd64],
 libmagick++-6.q16-dev,
 libacl1-dev,
 liboss4-salsa-dev [hurd-i386 kfreebsd-i386 kfreebsd-amd64],
 zlib1g-dev
Homepage: http://www.gnu.org/software/emacs/
Standards-Version: 3.7.2

Package: emacs25-lucid
Architecture: any
Depends: emacs25-bin-common (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Provides: emacs25, emacsen, editor, info-browser, mail-reader, news-reader
Suggests: emacs25-common-non-dfsg
Conflicts: emacs25, emacs25-nox
Replaces: emacs25, emacs25-nox
Description: GNU Emacs editor (with Lucid GUI support)
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains a version of Emacs with support for a graphical user
 interface based on the Lucid toolkit (instead of the GTK+ interface
 provided by the emacs25 package).  Until some known GTK+
 problems are fixed, this version may help avoid crashing Emacs during
 an emacsclient disconnect.  See
 https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=567934 and
 https://bugzilla.gnome.org/show_bug.cgi?id=85715 for more
 information.

Package: emacs25-lucid-dbg
Section: debug
Priority: extra
Architecture: any
Depends: emacs25-lucid-dbg (= ${binary:Version}), ${misc:Depends}
Description: Debugging symbols for emacs25-lucid
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains the debugging symbols for the emacs25-lucid
 package.

Package: emacs25-nox
Architecture: any
Depends: emacs25-bin-common (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Provides: emacs25, editor, emacsen, info-browser, mail-reader, news-reader
Suggests: emacs25-common-non-dfsg
Conflicts: emacs25, emacs25-lucid
Replaces: emacs25, emacs25-lucid
Description: GNU Emacs editor (without GUI support)
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains a version of Emacs compiled without support for X,
 and provides only a text terminal interface.

Package: emacs25-nox-dbg
Section: debug
Priority: extra
Architecture: any
Depends: emacs25-nox-dbg (= ${binary:Version}), ${misc:Depends}
Description: Debugging symbols for emacs25-nox
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains the debugging symbols for the emacs25-nox
 package.

Package: emacs25
Architecture: any
Depends: emacs25-bin-common (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Provides: editor, emacsen, info-browser, mail-reader, news-reader
Suggests: emacs25-common-non-dfsg
Conflicts: emacs25-lucid, emacs25-nox
Replaces: emacs25-lucid, emacs25-nox
Description: GNU Emacs editor (with GTK+ GUI support)
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains a version of Emacs with a graphical user interface
 based on GTK+ (instead of the Lucid toolkit provided by the
 emacs25-lucid package).

Package: emacs25-dbg
Section: debug
Priority: extra
Architecture: any
Depends: emacs25-dbg (= ${binary:Version}), ${misc:Depends}
Description: Debugging symbols for emacs25
 GNU Emacs is the extensible self-documenting text editor.  This
 package contains the debugging symbols for the emacs25 package.

Package: emacs25-bin-common
Architecture: any
Depends: emacs25-common (= ${source:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: GNU Emacs editor's shared, architecture dependent files
 GNU Emacs is the extensible self-documenting text editor.
 This package contains the architecture dependent infrastructure
 that's shared by emacs25, emacs25-lucid, and emacs25-nox.

Package: emacs25-common
Architecture: all
Depends: emacsen-common (>= 2.0.8), install-info, ${shlibs:Depends}, ${misc:Depends}
Recommends: emacs25-el
Suggests: emacs25-el, emacs25-common-non-dfsg, ncurses-term
Conflicts: emacs25-el (<< ${source:Version}), cedet, eieio, speedbar, gnus-bonus-el
Breaks: apel (<< 10.8+0.20120427-4)
Description: GNU Emacs editor's shared, architecture independent infrastructure
 GNU Emacs is the extensible self-documenting text editor.
 This package contains the architecture independent infrastructure
 that's shared by emacs25, emacs25-lucid, and emacs25-nox.

Package: emacs25-el
Architecture: all
Depends: emacs25-common (= ${source:Version}), ${misc:Depends}
Description: GNU Emacs LISP (.el) files
 GNU Emacs is the extensible self-documenting text editor.
 This package contains the elisp sources for the convenience of users,
 saving space in the main package for small systems.
